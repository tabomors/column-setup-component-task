## Installation guide:

Development:

```
npm i
npm start

```

It will spin up a server on http://localhost:3000/

Change `*.css` and `*.js` files in src folder to see livereloading

Build for production:

```
npm i
npm run build
```

It will give you minimized js and css

## Used technologies:

* Gulp (for task management) and bunch of plugins for gulp
* BrowserSync (for live reloading)
* React (View layer)
* react-beautiful-dnd (for d&d between lists)
* React Bootstrap (for grids mostly)

### P.S

One of the requirements was to organize this repo as Node.js project. So, i added simple express server to serve static files from dist folder if you want

Run:

```
npm run build && npm run publish
```
