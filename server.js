const express = require('express');
const path = require('path');

const app = express();

app.use(express.static(path.join(__dirname, 'dist')));

app.get('/', (req, res) => {
  res.sendFile(__dirname, path.join('dist', 'index.html'));
});

app.listen(9000, () => {
  console.log('Please check http://localhost:9000/');
});
