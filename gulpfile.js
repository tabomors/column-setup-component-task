const gulp = require('gulp');
const source = require('vinyl-source-stream');
const buffer = require('vinyl-buffer');
const gutil = require('gulp-util');
const browserify = require('browserify');
const babel = require('gulp-babel');
const del = require('del');
const browserSync = require('browser-sync').create();
const concat = require('gulp-concat');
const cleanCSS = require('gulp-clean-css');
const uglify = require('gulp-uglify');

const { watch, series, dest } = gulp;

function js() {
  return browserify('./dist/transpiled/index.js')
    .bundle()
    .on('error', gutil.log)
    .pipe(source('bundle.js'))
    .pipe(buffer())
    .pipe(dest('./dist/.'));
}

function minifyJs() {
  return gulp
    .src('./dist/bundle.js')
    .pipe(uglify())
    .pipe(gulp.dest('./dist'));
}

function transform() {
  return gulp
    .src('./src/**/*.js')
    .pipe(
      babel({
        presets: ['@babel/preset-env', '@babel/preset-react'],
        plugins: ['@babel/plugin-proposal-class-properties']
      })
    )
    .pipe(dest('./dist/transpiled'));
}

function clean(...folders) {
  return function clean(cb) {
    return del(folders, cb);
  };
}

function reloadBrowser(done) {
  browserSync.reload();
  done();
}

function setupServer(done) {
  browserSync.init({
    server: { baseDir: './dist' }
  });
  done();
}

function setupWatchers() {
  watch('./src/**/*.js', series(transform, js, reloadBrowser, clean('./dist/transpiled')));
  watch('./src/**/*.css', series(concatCss, reloadBrowser));
}

function moveHtml() {
  return gulp.src('./src/index.html').pipe(dest('./dist/.'));
}

function concatCss() {
  return gulp
    .src('./src/**/*.css')
    .pipe(concat('main.css'))
    .pipe(dest('./dist/.'));
}

function minifyCss() {
  return gulp
    .src('./dist/main.css')
    .pipe(cleanCSS())
    .pipe(gulp.dest('./dist'));
}

function defaultTask() {
  return series(
    clean('./dist'),
    moveHtml,
    transform,
    js,
    clean('./dist/transpiled'),
    concatCss,
    setupServer,
    setupWatchers
  );
}

function buildTask() {
  return series(
    clean('./dist'),
    moveHtml,
    transform,
    js,
    clean('./dist/transpiled'),
    minifyJs,
    concatCss,
    minifyCss
  );
}

module.exports = { default: defaultTask(), build: buildTask() };
