import React from 'react';
import { Droppable } from 'react-beautiful-dnd';
import CustomSetupItem from './CustomSetupItem';

function generateClassName({ isDraggingOver, withSeparator }) {
  const defaultClass = 'custom-setup-column';
  const draggingClass = `${defaultClass}--dragging`;
  const withSeparatorClass = withSeparator ? ' custom-setup-column--with-separator' : '';
  return (
    (isDraggingOver ? `${defaultClass} ${draggingClass}` : defaultClass) + withSeparatorClass
  );
}

export default function CustomSetupColumn({ items, id, title, withSeparator, ...otherProps }) {
  return (
    <>
      {title && <h3 className="custom-setup-column__title">{title}</h3>}
      <Droppable droppableId={id}>
        {(provided, snapshot) => (
          <div
            ref={provided.innerRef}
            className={generateClassName({ ...snapshot, withSeparator })}
          >
            {items.map((item, index) => (
              <CustomSetupItem
                item={item}
                index={index}
                key={item.id}
                column={id}
                {...otherProps}
              />
            ))}
            {provided.placeholder}
          </div>
        )}
      </Droppable>
    </>
  );
}
