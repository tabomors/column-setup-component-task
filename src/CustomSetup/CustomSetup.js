import React, { Component } from 'react';
import { DragDropContext } from 'react-beautiful-dnd';
import CustomSetupColumn from './CustomSetupColumn';

// Bootstrap stuff
import Button from 'react-bootstrap/Button';
import ButtonToolbar from 'react-bootstrap/ButtonToolbar';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';

function calculateInitialState({ visible, available, countOfFixedColumns }) {
  // TODO:
  // I don't quite understand the requirement
  // - and a number of fixed columns (fixed columns are shown with lock icon).
  // Is it just a count of fixed fields no matter what these fields are?
  // For now i assume that first {countOfFixedColumns} visible columns are fixed
  const distOfVisibleIds = visible.reduce((acc, item) => Object.assign(acc, { [item]: item }), {});
  const makeFixed = (item, index) =>
    index < countOfFixedColumns ? { ...item, isFixed: true } : item;
  const isVisible = ({ id }) => distOfVisibleIds[id];
  const visibleState = available.filter(isVisible).map(makeFixed);
  const availableState = available.filter(item => !isVisible(item));
  return {
    available: availableState,
    visible: visibleState
  };
}

export default class CustomSetup extends Component {
  constructor(props) {
    super(props);
    const { available, visible } = calculateInitialState(props);
    this.state = { available, visible };
  }

  onDragEnd = result => {
    const { source, destination } = result;

    // Dropped outside the list
    if (!destination) return;

    // Moved to the same list
    if (source.droppableId === destination.droppableId) {
      const listDropToId = source.droppableId;
      const itemsCopy = [...this.state[listDropToId]];

      const currentPosition = source.index;
      const nextPosition = destination.index;

      const [itemToMove] = itemsCopy.splice(currentPosition, 1);
      itemsCopy.splice(nextPosition, 0, itemToMove);

      const nextState = { [listDropToId]: itemsCopy };
      this.setState(nextState);
    }
    // Moved to the another list
    else {
      const listFromDragId = source.droppableId;
      const listToDragId = destination.droppableId;

      const listFromDragCopy = [...this.state[listFromDragId]];
      const listToDragCopy = [...this.state[listToDragId]];

      const currentPosition = source.index;
      const nextPosition = destination.index;

      const [itemToMove] = listFromDragCopy.splice(currentPosition, 1);
      listToDragCopy.splice(nextPosition, 0, itemToMove);
      const nextState = {
        [listFromDragId]: listFromDragCopy,
        [listToDragId]: listToDragCopy
      };
      this.setState(nextState);
    }
  };

  handleLock = ({ isFixed }, index, column) => {
    const columnItems = this.state[column];
    const updatedItems = isFixed
      ? columnItems.map((item, i) => (i >= index ? { ...item, isFixed: false } : item))
      : columnItems.map((item, i) => (i <= index ? { ...item, isFixed: true } : item));

    const newState = {
      ...this.state,
      [column]: updatedItems
    };
    this.setState(newState);
  };

  calculateResult = () => {
    const idsOfVisibleItems = this.state.visible.map(({ id }) => id);
    const fixedCount = this.state.visible.filter(({ isFixed }) => isFixed).length;

    const msg = `The resulting array of ids of visible columns is ${idsOfVisibleItems.join(
      ', '
    )} and a new number of fixed columns is ${fixedCount}`;

    // TODO: it could be a much nicer popup than rude alert
    alert(msg);
  };

  render() {
    const { available, visible } = this.state;
    return (
      <Container className="pt-5">
        <Row>
          <Col>
            <header className="custom-setup-header">
              <h1 className="custom-setup-header__title">Configure Data Fields</h1>
              <strong className="custom-setup-header__sub-title">
                Drag & drop between columns to configure visible data.
              </strong>
            </header>
          </Col>
        </Row>
        <Row className="mb-5">
          <DragDropContext onDragEnd={this.onDragEnd}>
            <Col className="mr-3">
              <CustomSetupColumn items={available} id="available" title="Available" withSeparator />
            </Col>
            <Col>
              <CustomSetupColumn
                items={visible}
                id="visible"
                title="Visible"
                handleLock={this.handleLock}
              />
            </Col>
          </DragDropContext>
        </Row>
        <Row>
          <Col>
            <ButtonToolbar>
              <Button variant="primary" size="lg" className="mr-2" onClick={this.calculateResult}>
                Save
              </Button>
              <Button variant="secondary" size="lg">
                Cancel
              </Button>
            </ButtonToolbar>
          </Col>
        </Row>
      </Container>
    );
  }
}
