import React from 'react';

import CustomSetup from './CustomSetup';

const availableData = [
  { id: 'startTime', name: 'Start Time' },
  { id: 'stopTime', name: 'Stop Time' },
  { id: 'perPoint', name: 'Per Point' },
  { id: 'initialMargin', name: 'Initial Margin' },
  { id: 'symbolAndDescription', name: 'Symbol & Description' },
  { id: 'changePercent', name: 'Change %' },
  { id: 'change', name: 'Change' },
  { id: 'last', name: 'Last' },
  { id: 'bidSize', name: 'Bid Size' },
  { id: 'lastVolume', name: 'Last Volume' },
  { id: 'bid', name: 'Bid' },
  { id: 'ask', name: 'Ask Size' },
  { id: 'totalVolume', name: 'Total Volume' },
  { id: 'high', name: 'High' }
];

const visibleData = [
  'symbolAndDescription',
  'changePercent',
  'change',
  'last',
  'lastVolume',
  'bid',
  'bidSize',
  'ask',
  'totalVolume',
  'high'
];

function App() {
  return <CustomSetup available={availableData} visible={visibleData} countOfFixedColumns={1} />;
}

export default App;

